class store:
    """ Implements an MDD store. 
        Designed for correctness not speed.
        Justin Pearson: Oct 2014.
    """
    def __init__(self):
        self.node_store = []
    def empty(self):
        return(True)
    def truenode(self):
        """ Returns the pointer of the true node.
            Note that we don't actually store the true node, it is just at -1.
        """
        return(-1)
    def node(self,pointer_id):
        """ Returns the actual node pointed to by pointer_id. 
            No sainity checking. Execeptions will be rasied. """
        return(self.node_store[pointer_id])
    def addnode(self,node):
        """ Adds the node if it is not there. If it is there it simply
            returns the pointer to the node. 
            raises an exception if the levels are the wrong way around."""
        node_pointer = -2
        for i in range(0,len(self.node_store)):
            current_node = self.node_store[i]
            if node.equal(current_node):
                node_pointer = i
                break
        if (node_pointer == -2):
            # We have to check that we are not pointing to any non-existant nodes.
            # and check that the level of the node we are adding is strictly
            # greater than the level of its children.
            for i in node.children():
                sub_pointer = i[1]
                if not (sub_pointer >= -1 and sub_pointer < len(self.node_store) ):
                    raise NameError('Attempting to add a node pointing to a non-existant node.')
                if sub_pointer == self.truenode():
                    if not (node.level() > self.truenode()):
                        raise NameError('Attempting to add wrong level.')
                else:
                    if not (node.level() > (self.node_store)[sub_pointer].level()):
                        raise NameError('Attempting to add wrong level.')
            self.node_store.append(node)
            node_pointer = len(self.node_store) - 1
        return(node_pointer)
    def size(self):
        return(len(self.node_store))
    def addnode_from_tuple(self,node_tuple):
        """Takes a list of sets [x_n-1,x_{n_2},...,x_0] 
         and constructs an that represents the cartesian product of
         all the in the lest. The last element of the set is assumed
         to be level 0 and so on.
         Does not function properly on empty lists.
        """
        node_tuple.reverse()
        previous_node = self.truenode()
        for i in range(0,len(node_tuple)):
            new_node = node(i,[(node_tuple[i],previous_node)])
            previous_node = self.addnode(new_node)
        node_tuple.reverse()
        return(previous_node)
    def cartesian(self,node_id):
        """ Returns the sorted cartesian product represented by the 
            node node_id.
        """
        if node_id == self.truenode():
            return([])
        #If we get here the node is not the truenode.
        current_node     = self.node(node_id)
        current_children = current_node.children()
        #Go through the children making the cartiesian product
        to_return = []
        for child in current_children:
            values     = child[0]
            child_node = child[1]
            for v in values:
                sub_list = self.cartesian(child_node)
                new_sub_list = []
                for s in sub_list:
                    new_item = [v] + s
                    to_return.append(new_item)
                if (sub_list == []):
                    to_return.append([v])
        to_return.sort()
        return(to_return)
    def from_cartesian(self,product):
        """ Creats a MDD representing the set in product.
        """
        current_node = self.truenode()
        for i in product:
            new_list = []
            for j in i:
                new_list.append(set([j]))
            new_node = self.addnode_from_tuple(new_list)
            current_node = self.union(current_node,new_node)
        return(current_node)
    def relation_from_cartesian(self,rel_tuples,positions):
        """ Constructs a relation from rel_tuples.
            positions is a list of the same langth as the members of
            rel_tuples. It tells you which variabels the relation is
            over. We start numbering at 0. We also assume that 
            positions is sorted.
        """
        current_node = self.truenode()
        max_position = positions[len(positions)-1]
        for i in rel_tuples:
            new_list = []
            current_tuple = list(i)
            current_pos = list(positions)
            for j in range(0,max_position+1):
                if j == current_pos[0]:
                    new_list.append(set([current_tuple[0]]))
                    current_tuple = current_tuple[1:len(current_tuple)]
                    current_pos = current_pos[1:len(positions)]
                else:
                    new_list.append(self.domain_size)
            new_node = self.addnode_from_tuple(new_list)
            current_node = self.union(current_node,new_node)
        return(current_node)
    
    def partition(self,children1,children2):
        """ Returns parition that refines children1 and
            children2 with the appropriate node pointers. """
        part_to_return = []
        union_all_1 = set([])
        for i in children1:
            iset = i[0]
            union_all_1 = union_all_1.union(iset)
        union_all_2 = set([])
        #Compute union of all 2
        for i in children2:
            iset = i[0]
            union_all_2 = union_all_2.union(iset)
        only_1 = union_all_1 - union_all_2
        only_2 = union_all_2 - union_all_1
        #Compute only2
        for i in children2:
            iset = i[0]
            i_only_2 = iset & only_2
            if len(i_only_2) > 0:
                part_to_return.append( (i_only_2,None,i[1]))
        #Compute only1
        for i in children1:
            iset = i[0]
            i_only_1 = iset & only_1
            if len(i_only_1) > 0:
                part_to_return.append( (i_only_1,i[1],None))
        #Compute all intersections.                                      
        for i in children1:
            for j in children2:
                child1 = i[1]
                child2 = j[1]
                set1   = i[0]
                set2   = j[0]
                r_set = set1 & set2
                if len(r_set) > 0:
                    part_to_return.append( (set1 & set2, child1,child2) )
        #sort and return
        part_to_return.sort(key=lambda tuple:list(tuple[0]))
        return(part_to_return)
    def union(self,node1,node2):
        """ Constructs the union node. """
        return(self.__genop(node1,node2,'union'))
    def intersection(self,node1,node2):
        """ Constructs the intersection node. """
#        print("In intersection ")
        return(self.__genop(node1,node2,'intersection'))
    
    def __genop(self,node1,node2,operation):
        """ Implements union or intersection"""
#        print("In genop operation = ",operation)
#        print("node1 = ",node1)
#        print("node2 = ",node2)
        if node1 == node2:
            return(node1)
        if self.node(node1).level() == self.node(node2).level():
#            print("levels are the same.")
            return(self.__genop_same_level(node1,node2,operation))
        #From now on in the code we can assume that the nodes
        #are at diffent levels.
        if self.node(node1).level() > self.node(node2).level():
            higher_node = node1
            lower_node  = node2
        else:
            higher_node = node2
            lower_node  = node1
        #Now construct a new node with the higher level, but with a single
        #child pointer at the lower node.
        higher_level = self.node(higher_node).level()
        new_node = node(higher_level,[(self.domain_size,lower_node)])
        new_node_id = self.addnode(new_node)
        new_union = self.__genop(higher_node,new_node_id,operation)
        return(new_union)
    
    def __genop_same_level(self,node1,node2,operation):
        """ Implements union or intersection. Assumes that the leves of the 
            nodes are the same."""
        #One base case for union.
        if operation == 'union':
            if node1 == self.truenode():
                return(node2)
            if node2 == self.truenode():
                return(node1)
        if operation == 'intersection':
            if node1 == self.truenode() or node2 == self.truenode():
#                print("In intersection base case")
                return self.truenode()
        children1 = self.node(node1).children()
        children2 = self.node(node2).children()
#        print("child1 = ",children1)
#        print("child2 = ",children2)
        part = self.partition(children1,children2)
#        print("paritiion = ",part)
        my_children = []
        for i in part:
            if i[1] == None and operation == 'union':
                my_children.append((i[0],i[2]))
            if i[2] == None and operation == 'union':
                my_children.append((i[0],i[1]))
            if (i[1] != None) and (i[2] != None):
#                print("creating new child call __genop again.")
                new_child = self.__genop(i[1],i[2],operation)
#                print("Back up a level.")
                if operation == 'union':
                    my_children.append((i[0],new_child))
                if (operation == 'intersection' and
                    new_child != self.truenode()):
                    my_children.append((i[0],new_child))
            if ( i[1] == i[2] == self.truenode()
                 and operation == 'intersection'
            ):
                my_children.append((i[0],self.truenode()))
#        print("my_children = ",my_children)
        if len(my_children) > 0:
            level = self.node(node1).level()
            new_node = node(level,my_children)
            new_id = self.addnode(new_node)
        else:
#            print("going to return truenode.")
            new_id = self.truenode()
#        print("new_id = ",new_id)
        return(new_id)
        
   

class node:
    """ Implemts a node.
    """
    def __init__(self,level=None,passed_children=None):
        if(level==None) or (passed_children==None):
            raise NameError('node: no level or children specicified')
        self.__subnodes = passed_children
        self.__subnodes.sort(key=lambda tuple:list(tuple[0]))
        self.__level = level
    def children(self):
        return(self.__subnodes)
    def size(self):
        return(len(self.__subnodes))
    def level(self):
        return(self.__level)
    def equal(self,other_node):
        if(self.size() != other_node.size()):
            return(False)
        if(self.level() != other_node.level()):
            return(False)
                
        else:
            return_value = True
            for i in range(0,self.size()):
                if self.__subnodes[i] != other_node.__subnodes[i]:
                    return_value = False
                    break
        return(return_value)

