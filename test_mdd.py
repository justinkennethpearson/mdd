""" Intersection and union seem to be working. Do more tests.
    Write a random testing framework. 
    The logic in __genop seems a bit convoluted. I think you can improve
    it by thinking harder. 
"""

import mdd
import unittest


def list_equal(l1,l2):
    if(len(l1) != len(l2)):
        return(False)
    else:
        return_value = True
        for i in range(0,len(l1)):
            if l1[i] != l2[i]:
                return_value = False
    return(return_value)

class Test_list_equal(unittest.TestCase):
    def test_equal(self):
        self.assertTrue(list_equal([],[]))
        self.assertTrue(list_equal([1,2],[1,2]))
        self.assertFalse(list_equal([1],[1,2,3,4]))
            

class TestMDD(unittest.TestCase):
    def test_init(self):
        store = mdd.store()
    def test_empty(self):
        store = mdd.store()
        self.assertTrue(store.empty())
    def test_addnode(self):
        """ Test adding a node """
        store = mdd.store()
        node  = mdd.node(0,[(set([1,2]),store.truenode())])
        new_node_id = store.addnode(node)
        self.assertTrue(store.size() == 1)
    #If we add the same node twice then it should only be there once.
        new_new_node_id = store.addnode(node)
        self.assertTrue(store.size() == 1)
        self.assertEqual(new_node_id,new_new_node_id)
    def test_addnode_invalid_children(self):
        """ A test that sees if we get an exception when add a node to the 
            the store that has children that are not in the store. """
        store = mdd.store()
        node  = mdd.node(0,[(set([1,2]),21)])
        #This node should not exists in an empty store.
        #So an exception should be raised.
        self.assertRaises(NameError,store.addnode,node)
    def test_addnode_invalid_level(self):
        """ Test if we try to add a node above a node with the levels in the
            wrong order. Levels must be strictly increasing as you go up from
            True.
        """
        store = mdd.store()
        node  = mdd.node(1,[(set([1,2]),store.truenode())])
        new_id = store.addnode(node)
        node_above = mdd.node(0,[(set([1,2]),new_id)])
        #store.addnode(node_above) should raise an exception.
        self.assertRaises(NameError,store.addnode,node_above)
    def test_get_node(self):
        """ We only get node identifiers. Test if we can get the actual nodes. """
        store = mdd.store()
        test_children = [(set([1,2]),store.truenode())]
        node  = mdd.node(0,test_children)
        new_node_id = store.addnode(node)
        actual_node = store.node(new_node_id)
        self.assertEqual(actual_node.level(),0)
        self.assertTrue(list_equal(actual_node.children(),test_children))
    def test_node_from_tuple(self):
        """ Test if we can create a node from a tuple. """
        store      =  mdd.store()
        node_tuple =  [set([1,2]),set([1,2,3]),set([1])]
        new_id = store.addnode_from_tuple(node_tuple)
        new_id_children = store.node(new_id).children()
        self.assertEqual(len(new_id_children),1)
        self.assertEqual(new_id_children[0][0],set([1,2]))
        child_id = new_id_children[0][1]
        grandchild = store.node(child_id)
        self.assertEqual(len(grandchild.children()),1)
        self.assertEqual(grandchild.children()[0][0],set([1,2,3]))
        greatgrandchildnode = store.node(grandchild.children()[0][1])
        greatgrandchild = greatgrandchildnode.children()
        expected_list = [(set([1]),store.truenode() )]
        self.assertEqual(greatgrandchild,expected_list)
    def test_sharing(self):
        """ Simple test for sharing. """
        store = mdd.store()
        id_1 = store.addnode_from_tuple( [set([1,2]),set([1]) ])
        id_2 = store.addnode_from_tuple( [set([7,8]),set([1]) ])
        #Both id_1 and id_2 should have the same children.
        id_1_children = store.node(id_1).children()
        id_2_children = store.node(id_2).children()
        #I should really rewrite the children api.
        id_1_child = id_1_children[0][1]
        id_2_child = id_2_children[0][1]
        self.assertEqual(id_1_child,id_2_child)        
    def test_cartesian_product_trivial(self):
        """ Checks that we can return empty lists. """
        store = mdd.store()
        product = store.cartesian(store.truenode())
        self.assertEqual(product,[])

    def test_cartesian_product_simple(self):
        store = mdd.store()
        node_tuple = [set([1])]
        new_id = store.addnode_from_tuple(node_tuple)
        product = store.cartesian(new_id)
        expected_products = [[1]]
    def test_cartesian_product_not_so_simple(self):
        store = mdd.store()
        node_tuple = [set([2]),set([1])]
        new_id = store.addnode_from_tuple(node_tuple)
        product = store.cartesian(new_id)
        expected_products = [[2,1]]
        self.assertEqual(product,expected_products)
        self.assertEqual(product,expected_products)
        
    def test_cartesian_product(self):        
        """ Test if we can create a node from a tuple and
            then look at its cartesian product"""
        store      =  mdd.store()
        node_tuple =  [set([1,2]),set([1,2,3]),set([1])]
        new_id  = store.addnode_from_tuple(node_tuple)
        product = store.cartesian(new_id)
        expected_product = [[1,1,1],[1,2,1],[1,3,1],
                                [2,1,1],[2,2,1],[2,3,1]]
        expected_product.sort()
        self.assertEqual(product,expected_product)
    def test_simple_1_from_cartesian_product(self):
        expected_product = [[1,1,1]]
        store   = mdd.store()
        new_id  = store.from_cartesian(expected_product)
        product = store.cartesian(new_id)
        expected_product.sort()
        self.assertEqual(expected_product,product)
    def test_simple_2_from_cartesian_product(self):
        expected_product = [[1,1,1],[1,2,1],[1,3,1]]
        store   = mdd.store()
        new_id  = store.from_cartesian(expected_product)
        product = store.cartesian(new_id)
        expected_product.sort()
        product.sort()
        self.assertEqual(expected_product,product)
    def test_simple_3_from_cartesian_product(self):
        """ Test if union is idempotent. """ 
        non_ido_expected_product = [[1,1,1],[1,1,1]]
        expected_product = [[1,1,1]]
        store   = mdd.store()
        new_id  = store.from_cartesian(non_ido_expected_product)
        product = store.cartesian(new_id)
        expected_product.sort()
        product.sort()
        self.assertEqual(expected_product,product)
    def test_simple_4_cartesian_product(self):
        expected_product = [[1,1,1],[1,2,1],[1,3,1],
                                [2,1,1],[2,2,1]]
        store   = mdd.store()
        new_id  = store.from_cartesian(expected_product)
        product = store.cartesian(new_id)
        expected_product.sort()
        product.sort()
        self.assertEqual(expected_product,product)

    def test_simple_5_cartesian_product(self):
        expected_product = [[1,1,1],[1,2,1],[1,3,1],
                                [2,1,1]]
        store   = mdd.store()
        new_id  = store.from_cartesian(expected_product)
        product = store.cartesian(new_id)
        expected_product.sort()
        product.sort()
        self.assertEqual(expected_product,product)
    def test_simple_6_cartesian_product(self):
        union1 = [ [1,1,1] ,
                  [2,1,1]]
        expected_product = union1 + [[2,2,1]]
        store   = mdd.store()
        new_id_1  = store.from_cartesian(union1)
        new_id_2  = store.from_cartesian([[2,2,1]])
        new_id = store.union(new_id_1,new_id_2)
        product = store.cartesian(new_id)
        expected_product.sort()
        product.sort()
        self.assertEqual(expected_product,product)
    def test_relation_from_cartesian(self):
        store = mdd.store()
        store.domain_size = set([1,2,3])
        rel_tuples = [[1,2],[2,3]]
        new_node = store.relation_from_cartesian(rel_tuples,[1,2])
        expected_product = [[1,1,2],[2,1,2],[3,1,2],[1,2,3],[2,2,3],[3,2,3]]
        expected_product.sort()
        product = store.cartesian(new_node)
        self.assertEqual(expected_product,product)
        
    def test_union_from_6(self):
        store  = mdd.store()
        node_1 = store.addnode_from_tuple([set([1,2]),set([1]),set([1])])
        node_2 = store.addnode_from_tuple([set([2]),  set([2]),set([1])])
        union_node = store.union(node_1,node_2)
        expected_product = [[1,1,1],[2,1,1],[2,2,1]]
        expected_product.sort()
        product = store.cartesian(union_node)
        product.sort()
        self.assertEqual(expected_product,product)
        
    def test_from_cartesian_product(self):
        expected_product = [[1,1,1],[1,2,1],[1,3,1],
                                [2,1,1],[2,2,1],[2,3,1]]
        store   = mdd.store()
        new_id  = store.from_cartesian(expected_product)
        product = store.cartesian(new_id)
        expected_product.sort()
        product.sort()
        self.assertEqual(expected_product,product)
        
    def test_partition_1(self):
        children1 = [(set([1,2,3]),0),(set([7,8]),1)]
        children2 = [(set([1,2]),2),(set([6,7]),3)]
        expected  = [(set([1,2]),0,2),(set([3]),0,None),(set([6]),None,3),
                     (set([7]),1,3),(set([8]),1,None)]
        store = mdd.store()
        part  = store.partition(children1,children2)
        self.assertEqual(part,expected)
    def test_partition_2(self):
        children1 = [(set([1,2]),0)]
        children2 = [(set([2]),1)]
        expected  = [(set([1]),0,None),(set([2]),0,1)]
        expected.sort(key=lambda tuple:list(tuple[0]))
        store = mdd.store()
        part  = store.partition(children1,children2)
        self.assertEqual(part,expected)        
    def test_simple_union(self):
        store = mdd.store()
        node_tuple_1   = [set([1]),set([1])]
        node_1         = store.addnode_from_tuple(node_tuple_1)
        node_tuple_2   = [set([2]),set([1,2])]
        node_2         = store.addnode_from_tuple(node_tuple_2)
        union_node     = store.union(node_1,node_2)
        product        = store.cartesian(union_node)
        expected_product = [[1,1],[2,1],[2,2]]
        expected_product.sort()
        self.assertEqual(product,expected_product)
    def test_union_different_levels(self):
        store = mdd.store()
        store.domain_size = set([1,2,3])
        node_tuple_1   = [set([1])]
        node_1         = store.addnode_from_tuple(node_tuple_1)
        node_tuple_2   = [set([2]),set([2,1])]
        node_2         = store.addnode_from_tuple(node_tuple_2)
        union_node     = store.union(node_1,node_2)
        product        = store.cartesian(union_node)
        expected_product = [[1,1],[2,1],[3,1],[2,2]]
        expected_product.sort()
        self.assertEqual(product,expected_product)
    def test_intersection_simple_1(self):
        store = mdd.store()
        node_1 = store.addnode_from_tuple([set([1,2,3])])
        node_2 = store.addnode_from_tuple([set([1,2])])
        intersection_node = store.intersection(node_1,node_2)
        product = store.cartesian(intersection_node)
        expected_product = [[1],[2]]
        expected_product.sort()
        self.assertEqual(product,expected_product)
    def test_intersection_simple_2(self):
        store = mdd.store()
        node_1_set = [[1,1,1],[1,2,1],[1,3,1],[2,2,2]]
        node_2_set = [[1,1,1],[1,3,1],[2,2,2],[3,3,3]]
        node_1 = store.from_cartesian(node_1_set)
        node_2 = store.from_cartesian(node_2_set)
        intersection_node = store.intersection(node_1,node_2)
        returned_intersection = store.cartesian(intersection_node)
        expected = [[1,1,1],[1,3,1],[2,2,2]]
        expected.sort()        
        self.assertEqual(returned_intersection,expected)
    def test_intersection_empty_1(self):
        store = mdd.store()
        node_1_set = [[1]]
        node_2_set = [[2]]
        node_1 = store.from_cartesian(node_1_set)
        node_2 = store.from_cartesian(node_2_set)
        intersection_node = store.intersection(node_1,node_2)
        returned_intersection = store.cartesian(intersection_node)
        expected = []
        expected.sort()        
        self.assertEqual(returned_intersection,expected)
    def test_intersection_empty_2(self):
        store = mdd.store()
        node_1_set = [[1,2]]
        node_2_set = [[2,2]]
        node_1 = store.from_cartesian(node_1_set)
        node_2 = store.from_cartesian(node_2_set)
        intersection_node = store.intersection(node_1,node_2)
        returned_intersection = store.cartesian(intersection_node)
        expected = []
        expected.sort()        
        self.assertEqual(returned_intersection,expected)
    def test_intersection_empty_3(self):
        store = mdd.store()
        node_1_set = [[1,1],[2,2]]
        node_2_set = [[2,1]]
        node_1 = store.from_cartesian(node_1_set)
        node_2 = store.from_cartesian(node_2_set)
        intersection_node = store.intersection(node_1,node_2)                
        returned_intersection = store.cartesian(intersection_node)
        expected = []
        expected.sort()        
        self.assertEqual(returned_intersection,expected)
    def test_union_sainty_check_union_empty(self):
        store = mdd.store()
        node_1_set = [[1,1],[2,2]]
        node_2_set = [[2,1]]
        node_1 = store.from_cartesian(node_1_set)
        node_2 = store.from_cartesian(node_2_set)
        union_node = store.union(node_1,node_2)
        returned_union = store.cartesian(union_node)
        expected = node_1_set + node_2_set
        expected.sort()        
        self.assertEqual(returned_union,expected)
        

class TestNode(unittest.TestCase):
    """ We have a node class so that we can add them to the store."""
    def test_init_empty(self):
        """ We need to provide constructors. If none are provided
            then we should get an exception.
        """
        self.assertRaises(NameError,mdd.node)
    def test_init_list(self): 
        children = [(set([1,2,3]),0),(set([7,8]),1)]
        #Create a node with children.
        #Arguments level,children.
        node = mdd.node(0,children)
        return_children = node.children()
        self.assertTrue(list_equal(node.children(),children))
        self.assertEqual(node.level(),0)
    def test_init_list_normalise(self):
        children = [(set([7,8]),1),(set([1,2,3]),0)]
        children_norm = [(set([1,2,3]),0),(set([7,8]),1)]
        #Create a node with children.
        node = mdd.node(0,children)
        return_children = node.children()
        self.assertTrue(list_equal(node.children(),children_norm))
        
        
if __name__ == '__main__':
    unittest.main()
