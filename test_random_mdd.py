"""
 Very simple random testing framework for intersection and union. 
 random_test(domain_size,arit,notuples,notrials)
 Tests both intesection and union with randomly generated tuples.
"""

import mdd
import unittest
import random
import sys
def normalise(l):
    """ Remove dupicates and sorts. 
        Returns a new list. """
    new_list = []
    for i in l:
        in_list = False
        for j in new_list:
            if i==j:
                in_list = True
                break
        if not in_list:
            new_list.append(i)
    new_list.sort()
    return(new_list)


def union(l1,l2):
    new_list = l1 + l2
    return(normalise(new_list))
def intersection(l1,l2):
    new_list = []
    for i in l1:
        if member(i,l2):
            new_list.append(i)
    return(new_list)

def member(e,l1):
    e_in_l1 = False
    for i in l1:
        if  i==e:
            e_in_l1 = True
            break
    return(e_in_l1)

class Test_plumbing(unittest.TestCase):
    def test_normalise(self):
        l = [3,1,3,4]
        expected = [1,3,4]
        self.assertEqual(expected,normalise(l))
        self.assertEqual([],normalise([]))
        self.assertEqual([1],normalise([1]))
        more_l = [[1,2,3],[3,4,1],[1,2,3],[2,4,1]]
        more_expected = [[1,2,3],[3,4,1],[2,4,1]]
        more_expected.sort()
        self.assertEqual(more_expected,normalise(more_l))
    def test_union(self):
        l1 = [[1,2,3],[4,5,6]]
        l2 = [[3,4,2],[4,5,6],[9,8,7]]
        l1unionl2 = [[1,2,3],[4,5,6],[3,4,2],[9,8,7]]
        l1unionl2.sort()
        self.assertEqual(l1unionl2,union(l1,l2))
    def test_member(self):
        l1 = [[1,2,3],[4,5,6]]
        self.assertTrue(member([1,2,3],l1))
        self.assertFalse(member([3,2,1],l1))
        
    def test_intersection(self):
        l1 = [[1,2,3],[4,5,6]]
        l2 = [[3,4,2],[4,5,6],[9,8,7]]
        l1interl2 = [[4,5,6]]
        l1interl2.sort()
        self.assertEqual(l1interl2,intersection(l1,l2))
        self.assertEqual([],intersection([],[]))


def random_test_once(domain_size,arity,notuples):
    store = mdd.store()
    store.domain_size = set(range(domain_size))
    set1 = []
    set2 = []
    for i in range(notuples):
        tuple1 = []
        tuple2 = []
        for j in range(arity):
            tuple1.append(random.randrange(0,domain_size))
            tuple2.append(random.randrange(0,domain_size))
        set1.append(tuple1)
        set2.append(tuple2)
    set1 = normalise(set1)
    set2 = normalise(set2)
    expected_union        = union(set1,set2)
    expected_intersection = intersection(set1,set2)
    node_1 = store.from_cartesian(set1)
    node_2 = store.from_cartesian(set2)
    intersection_node = store.intersection(node_1,node_2)
    union_node     = store.union(node_1,node_2)
    returned_intersection = store.cartesian(intersection_node)
    returned_union  = store.cartesian(union_node)
    if returned_union == expected_union:
        sys.stdout.write('+')
    else:
        print("Error unexpected union")
        print("Expected ",expected_union)
        print("Got ",returned_union)
    if returned_intersection == expected_intersection:
        sys.stdout.write('&')
    else:
        print("Error unexpected intersection")
        print("Expected ",expected_intersection)
        print("Got ",returned_intersection)
    sys.stdout.flush()
    
def random_test(domain_size,arity,notuples,notrials):
    print("Start random testing.")
    for i in range(notrials):
        random_test_once(domain_size,arity,notuples)
    print("\nDone random testing.")

        
            
            
    
    
        


if __name__ == '__main__':
    random_test(10,10,70,100)
    unittest.main()

        
